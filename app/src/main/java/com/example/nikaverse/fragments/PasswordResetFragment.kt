package com.example.nikaverse.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.nikaverse.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth

class PasswordResetFragment : Fragment(R.layout.fragment_password_reset) {
    private lateinit var layoutEmail: TextInputLayout
    private lateinit var editTextEmail: TextInputEditText
    private lateinit var sendButton: Button
    private lateinit var backButton: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_password_reset, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        registerListeners()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true)
            {
                override fun handleOnBackPressed() {
                    val action = PasswordResetFragmentDirections.actionPasswordResetFragmentToLoginFragment()
                    findNavController().navigate(action)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }
    private fun init() {
        layoutEmail = view?.findViewById(R.id.layoutEmail)!!
        editTextEmail = view?.findViewById(R.id.editTextEmail)!!
        sendButton = view?.findViewById(R.id.buttonSend)!!
        backButton = view?.findViewById(R.id.backButton)!!
    }

    private fun registerListeners() {
        sendButton.setOnClickListener {
            val email = editTextEmail.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(activity, "Empty email!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(activity, "Check email!", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(activity, "Errotkh!", Toast.LENGTH_SHORT).show()
                    }
                }
        }
        backButton.setOnClickListener {
            val action =
                PasswordResetFragmentDirections.actionPasswordResetFragmentToLoginFragment()
            findNavController().navigate(action)
        }
    }
}