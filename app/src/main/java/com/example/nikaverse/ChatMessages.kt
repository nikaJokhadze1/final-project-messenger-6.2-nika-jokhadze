package com.example.nikaverse

class ChatMessages(
    val id: String,
    val text: String,
    val fromUser: String,
    val toUser: String,
    val timestamp: Long,
    val fromUid:String,
) {
    constructor() : this("", "", "", "", -1, "")
}
