package com.example.nikaverse

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigationMenu = findViewById<BottomNavigationView>(R.id.bottomNavMenu)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val controller = navHostFragment.navController
        bottomNavigationMenu.setupWithNavController(navController = controller) // setting up navigation bar

        // HIDING NAVIGATION BAR IN CERTAIN FRAGMENTS
        controller.addOnDestinationChangedListener{ navController: NavController, navDestination: NavDestination, bundle: Bundle? ->

            when (navDestination.id) {
                R.id.loginFragment , R.id.registrationFragment, R.id.passwordResetFragment, R.id.messageFragment, R.id.passwordChangeFragment-> bottomNavigationMenu.visibility = GONE

                else -> bottomNavigationMenu.visibility = VISIBLE

            }

        }
    }


}