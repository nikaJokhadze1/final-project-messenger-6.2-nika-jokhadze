package com.example.nikaverse.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.nikaverse.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : Fragment(R.layout.fragment_profile) {
    private lateinit var textViewUsername: TextView
    private lateinit var textViewMail: TextView
    private lateinit var buttonChange: Button
    val uid = FirebaseAuth.getInstance().uid
    val db = FirebaseDatabase.getInstance().getReference("/userinfo/$uid")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        registerListeners()
        getUserInfo()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val action = ProfileFragmentDirections.actionProfileFragmentToHomeFragment()
                    findNavController().navigate(action)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    private fun registerListeners() {
        buttonChange.setOnClickListener {
            val action = ProfileFragmentDirections.actionProfileFragmentToPasswordChangeFragment()
            findNavController().navigate(action)
        }
    }

    private fun init() {
        textViewUsername = view?.findViewById(R.id.textViewName)!!
        textViewMail = view?.findViewById(R.id.textViewEmail)!!
        buttonChange = view?.findViewById(R.id.buttonChangePassword)!!
    }

    private fun getUserInfo() {
        db.addListenerForSingleValueEvent(object :
            ValueEventListener { // informaciis wamogeba current user
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val username = dataSnapshot.child("username").getValue(String::class.java)
                val email = dataSnapshot.child("email").getValue(String::class.java)
                textViewUsername.text = username
                textViewMail.text = email

            }

            override fun onCancelled(error: DatabaseError) {
            }
        }
        )
    }


}