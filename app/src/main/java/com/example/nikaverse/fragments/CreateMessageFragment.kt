package com.example.nikaverse.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.nikaverse.R
import com.example.nikaverse.UserInfo
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class CreateMessageFragment : Fragment(R.layout.fragment_create_message) {
    private lateinit var recyclerView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getUsers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val action =
                        CreateMessageFragmentDirections.actionCreateMessageFragmentToHomeFragment()
                    findNavController().navigate(action)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    private fun init() {
        recyclerView = view?.findViewById(R.id.RecyclerView)!!

    }

    private fun getUsers() {
        val db = FirebaseDatabase.getInstance().getReference("/userinfo")
        db.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val adapter = GroupieAdapter()
                snapshot.children.forEach {
                    val user = it.getValue(UserInfo::class.java)
                    adapter.add(UserItem(user))
                    adapter.setOnItemClickListener { item, view ->
                        val username: TextView = view.findViewById(R.id.textViewUserNAME)
                        val argument = username.text.toString()
                        val action =
                            CreateMessageFragmentDirections.actionCreateMessageFragmentToMessageFragment(
                                argument
                            )
                        findNavController().navigate(action)
                    }
                    recyclerView.adapter = adapter

                }

            }

            override fun onCancelled(error: DatabaseError) {
            }

        })

    }

    class UserItem(val user: UserInfo?) : Item<GroupieViewHolder>() {
        override fun getLayout() = R.layout.users_create_message

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.findViewById<TextView>(R.id.textViewUserNAME).text =
                user?.username.toString()
        }
    }
}

